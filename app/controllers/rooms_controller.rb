class RoomsController < ApplicationController

  def upload
    uploaded_io = params[:file]
    name = params[:file].original_filename
    directory = "public/uploads/"+params[:room]
    # create the file path
    path = File.join(directory, name)
    # write the file
    File.open(path, "wb") { |f| f.write(uploaded_io.read) }
    #File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'w') do |file|
    #  file.write(uploaded_io.read)
    #end

    #send_file path, :type => 'image/png', :disposition => 'inline'#, :filename => params[:file].original_filename
    #,:type => "application/pdf"
    respond_to do |format|
      format.json {render :json => ''}
    end
  end



  def index
    @rooms = Room.where(:public => true).order("created_at DESC")
    @new_room = Room.new
  end

  def get_files_from_room
    #Dir.glob("/path/to/my/directory/**/*")
    #{"dirs":"'+Dir.glob("/uploads/"+params[:room]+"/**/*")+'"}
    dirs = Dir.entries("#{Rails.root.to_s}/public/uploads/"+params[:room].to_s)#Dir.glob("#{Rails.root.to_s}/public/uploads/22/**/*").to_s
    json_text = { :room => params[:room].to_s, :dirs => dirs }.to_json
    respond_to do |format|
      format.json {render :json => JSON.parse(json_text)}
      format.html
    end
  end

  def create
    config_opentok
    session = @opentok.create_session request.remote_addr
    params[:room][:sessionId] = session.session_id
    @new_room = Room.new(params[:room])
    respond_to do |format|
      if @new_room.save
        Dir.mkdir "#{Rails.root.to_s}/public/uploads/" +@new_room.id.to_s
        format.html { redirect_to("/party/"+@new_room.id.to_s) }
      else
        format.html { render :controller => 'rooms',
               :action => "index" }
      end
    end
  end

  def party
    @room = Room.find(params[:id])

    config_opentok

    @tok_token = @opentok.generate_token :session_id =>
          @room.sessionId
  end

  private
  def config_opentok
    if @opentok.nil?
      @opentok = OpenTok::OpenTokSDK.new 28878252, "ae478e8c6003c84324dd48305d6232b703454e62"
    end
  end


end